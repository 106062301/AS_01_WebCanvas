var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var Mode = 'brush';
var imageStack = [];
var imageRedo = [];
var start_x;
var start_y;
var text = [];
var textX;
var textY;
var texting = false;
var solid = false;
var cursors = [
  'brush_cursor.png',
  'eraser_cursor.png'
];

function init() {
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, 600, 410);
  ctx.fillStyle = 'black';
  ctx.lineJoin = ctx.lineCap = 'round';
  brush();
  document.getElementById('penswidth').value = 10;
  ctx.lineWidth = 10;
  var imgData = ctx.getImageData(0, 0, 650, 500);
  imageStack.push(imgData);
  document.getElementById("font-size").value = 12;
  document.getElementById("font-family").value = 'arial';
}

var x = 0;
var y = 0;
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.offsetX,
    y: evt.offsetY
  };
};
canvas.addEventListener('mousedown', function (evt) {
  var mousePos = getMousePos(canvas, evt);
  if (Mode == 'brush' || Mode == 'eraser') {
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y + 32);
    canvas.addEventListener('mousemove', mouseMove, false);
  } else if (Mode == 'text') {
    text = '';
    textX = mousePos.x;
    textY = mousePos.y;
    addText(textX, textY);
  } else if (Mode == 'rectangle' || Mode == 'circle' || Mode == 'triangle') {
    start_x = mousePos.x;
    start_y = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove, false);
  }
});
function mouseMove(evt) {
  if (Mode == 'brush' || Mode == 'eraser') {
    var mousePos = getMousePos(canvas, evt);
    ctx.lineTo(mousePos.x, mousePos.y + 32);
    ctx.stroke();
  } else if (Mode == 'rectangle') {
    drawRectangle(evt);
  } else if (Mode == 'circle') {
    drawCircle(evt);
  } else if (Mode == 'triangle') {
    drawTriangle(evt);
  }
};

canvas.addEventListener('mouseup', function () {
  var imgData = ctx.getImageData(0, 0, 650, 500);
  imageStack.push(imgData);
  imageRedo.splice(0, imageRedo.length);
  canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

function brush() {
  Mode = 'brush';
  document.getElementById('canvas').style.cursor = 'url("' + cursors[0] + '"), default';
  ctx.strokeStyle = 'black';
}

function eraser() {
  Mode = 'eraser';
  document.getElementById('canvas').style.cursor = 'url("' + cursors[1] + '"), default';
  ctx.strokeStyle = 'white';
}

function textMode() {
  Mode = 'text';
  document.getElementById('canvas').style.cursor = 'text';
}
function addText(x, y) {
  var inputText = document.createElement("input");
  inputText.id = "inputText";
  inputText.style.position = "absolute";
  inputText.style.left = `${x + 53}px`;
  inputText.style.top = `${y + 94}px`;
  var canvasArea = document.getElementById("canvasArea")
  canvasArea.appendChild(inputText);
  inputText.addEventListener("keydown", function (event) {
    if (event.key == 'Enter') {
      var text = inputText.value;
      canvasArea.removeChild(inputText);
      var fontSize = document.getElementById("font-size").value;
      var fontFamily = document.getElementById("font-family").value;
      ctx.font = `${fontSize}px ${fontFamily}`;
      ctx.fillText(text, x, y + 4);
      var imgData = ctx.getImageData(0, 0, 650, 500);
      imageStack.push(imgData);
      imageRedo.splice(0, imageRedo.length);
    }
  })
}
function rectangleMode() {
  Mode = 'rectangle';
  document.getElementById('canvas').style.cursor = 'crosshair';
}

function drawRectangle(evt) {
  var now_x = evt.clientX - canvas.offsetLeft - 4;
  var now_y = evt.clientY - canvas.offsetTop - 4;
  var imgData = imageStack[imageStack.length - 1];
  ctx.putImageData(imgData, 0, 0);
  if (solid) {
    ctx.fillRect(start_x, start_y, now_x - start_x, now_y - start_y);
  } else {
    ctx.strokeRect(start_x, start_y, now_x - start_x, now_y - start_y);
  }
}

function circleMode() {
  Mode = 'circle';
  document.getElementById('canvas').style.cursor = 'crosshair';
}

function drawCircle(evt) {
  var now_x = evt.x - start_x - 50;
  var now_y = evt.y - start_y - 116;
  ctx.beginPath();
  var imgData = imageStack[imageStack.length - 1];
  ctx.putImageData(imgData, 0, 0);
  ctx.arc(start_x, start_y, Math.sqrt(now_x * now_x + now_y * now_y), 0, 2 * Math.PI);
  if (solid) {
    ctx.fill();
  } else {
    ctx.stroke();
  }
}

function triangleMode() {
  Mode = 'triangle';
  document.getElementById('canvas').style.cursor = 'crosshair';
}

function drawTriangle(evt) {
  ctx.beginPath();
  var imgData = imageStack[imageStack.length - 1];
  ctx.putImageData(imgData, 0, 0);
  ctx.moveTo(
    (start_x + evt.x - 57) / 2,
    start_y
  );
  ctx.lineTo(
    start_x,
    evt.y - 106
  );
  ctx.lineTo(evt.x - 57, evt.y - 106);
  ctx.closePath();
  if (solid) {
    ctx.fill();
  } else {
    ctx.stroke();
  }
}

function Solid() {
  if (solid == false) {
    solid = true;
    document.getElementById("solid").src = "solid_icon.png";
  } else {
    solid = false;
    document.getElementById("solid").src = "circle_icon.png";
  }
}

function fillUp() {
  ctx.fillRect(0, 0, 600, 410);
  var imgData = ctx.getImageData(0, 0, 650, 500);
  imageStack.push(imgData);
  imageRedo.splice(0, imageRedo.length);
}

function undo() {
  if (imageStack.length > 1) {
    imageRedo.push(imageStack[imageStack.length - 1]);
    imageStack.pop();
    ctx.putImageData(imageStack[imageStack.length - 1], 0, 0);
  }
}
function redo() {
  if (imageRedo.length > 0) {
    imageStack.push(imageRedo[imageRedo.length - 1]);
    imageRedo.pop();
    ctx.putImageData(imageStack[imageStack.length - 1], 0, 0);
  }
}

function reset() {
  imageStack.splice(1, imageStack.length);
  imageRedo.splice(0, imageRedo.length);
  ctx.putImageData(imageStack[0], 0, 0);
}

function download() {
  var lnk = document.createElement('a'), e;
  lnk.download = 'myImage';
  lnk.href = canvas.toDataURL("image/png;base64");
  if (document.createEvent) {
    e = document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window,
      0, 0, 0, 0, 0, false, false, false,
      false, 0, null);

    lnk.dispatchEvent(e);
  } else if (lnk.fireEvent) {
    lnk.fireEvent("onclick");
  }
}

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', upload, false);

function upload(e) {
  var reader = new FileReader();
  reader.onload = function (event) {
    var img = new Image();
    img.onload = function () {
      ctx.drawImage(img, 0, 0, 600, 400);
    }
    img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);
  var imgData = ctx.getImageData(0, 0, 650, 500);
  imageStack.push(imgData);
  imageRedo.splice(0, imageRedo.length);
}

function changeColor(color) {
  if (Mode != 'eraser') {
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
  }
}

function changeWidth() {
  ctx.lineWidth = document.getElementById('penswidth').value;
}
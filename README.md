# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
    brush:  paint by moving the mouse while it is presssed.
    eraser: erease by moving the mouse while it is presssed.
    text:   click the mouse on the canvas.
            click the input box and type.
            Press enter to stick in onto the canvas.
    rectangel: press and pull to make a rectangle.
    triangle: press and pull to make a triangle.
    circle: press and pull to make a circle.
    solid: if the icon is filled it is solid and vise versa.
    fillup: fill the whole canvas with a color.
    undo: undo the step just made.
    redo: redo the step just undone.
    reset: clear the canvas.
    download: download the canvas as png.
    upload: upload a image on the canvas.
    width: change the width of brush, eraser,and shapes.
    fonts: change the font of texts.
    colorboard: change the color of brush, shape, and texts.
